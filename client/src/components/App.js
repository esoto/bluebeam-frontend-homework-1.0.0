import React from 'react';
import ReactDOM from 'react-dom';
import { hot } from 'react-hot-loader';

const App = () => {
  return (
    <div>
      Welcome to the Bluebeam Web Development Homework!!
    </div>
  );
};

export default hot(module)(App);
